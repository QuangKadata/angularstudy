export class Ingredient {
    'name': string;
    'amount': number;

    constructor(name: string,amout:number){
        this.amount = amout;
        this.name = name;
    }
}